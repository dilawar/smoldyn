// compile with gcc with: gcc -Wall hello.c -o hello
// compile with MinGW with: x86_64-w64-mingw32-gcc -Wall hello.c -o hello.exe

#include <stdio.h>
int main()
{
   // printf() displays the string inside quotation
   printf("Hello, World!\n");
   return 0;
}
