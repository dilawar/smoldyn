[![Build Status](https://travis-ci.org/dilawar/smoldyn.svg?branch=master)](https://travis-ci.org/dilawar/smoldyn)

__IMP__ This repository contains only the source code of smoldyn simulator. I 
have removed documentation and other assests. This is not the official clone of 
the simulator. This is here for testing packaging. 

# Packages

These are available here for various linux flavors https://software.opensuse.org//download.html?project=home%3Adilawar&package=smoldyn
